<?php
/**
* Modelo para el acceso a la base de datos y funciones CRUD
* Autor: ELivar Largo
* Sitio Web: wwww.ecodeup.com
*/
class Articulo
{
	
	//constructor de la clase
	function __construct($idarticulo,$plantilla, $titulo, $subtitulo, $categoria,$condicion,$descripcion,$cantidad,$precio,$idus,$imagen)
	{
		$this->idarticulo=$idarticulo;
		$this->plantilla=$plantilla;
		$this->titulo=$titulo;
		$this->subtitulo=$subtitulo;
		$this->categoria=$categoria;
		$this->condicion=$condicion;
		$this->descripcion=$descripcion;
		$this->cantidad=$cantidad;
		$this->precio=$precio;
		$this->idus=$idus;
		$this->imagen=$imagen;

	}
	

	//la función para registrar un usuario
	public static function save($articulo){
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO articulo VALUES(null,:plantilla,:titulo,:subtitulo,:categoria,:condicion,:descripcion,:cantidad,:precio,:idus,:imagen)');
		$insert->bindValue('plantilla',$articulo->plantilla);
		$insert->bindValue('titulo',$articulo->titulo);
		$insert->bindValue('subtitulo',$articulo->subtitulo);
		$insert->bindValue('categoria',$articulo->categoria);
		$insert->bindValue('condicion',$articulo->condicion);
		$insert->bindValue('descripcion',$articulo->descripcion);
		$insert->bindValue('cantidad',$articulo->cantidad);
		$insert->bindValue('precio',$articulo->precio);
		$insert->bindValue('idus',$articulo->idus);
		$insert->bindValue('imagen',$articulo->imagen);
		$insert->execute();
	}
	
	public function mostrar($id){
		require_once('../connection.php');
		$db=Db::getConnect();
		$listaArticulos=[];
		$select=$db->prepare('SELECT * FROM articulo WHERE idus=:id');
		$select->bindValue('id',$id);
		$select->execute();
		
		foreach($select->fetchAll() as $articulo){
			$myArticulo= new Articulo($articulo['idarticulo'],$articulo['plantilla'],$articulo['titulo'],$articulo['subtitulo'],$articulo['categoria'],$articulo['condicion'],$articulo['descripcion'],$articulo['cantidad'],$articulo['precio'],$articulo['idus'],$articulo['imagen']);
			$listaArticulos[]=$myArticulo;
		}
		return $listaArticulos;
	}
	public function buscarId($id)
	{
		require_once('../connection.php');
		$db=Db::getConnect();
		$select=$db->prepare('SELECT * FROM articulo WHERE idarticulo=:id');
		$select->bindValue('id',$id);
		$select->execute();

		$articulo = $select->fetch();
		$myArticulo= new Articulo($articulo['idarticulo'],$articulo['plantilla'],$articulo['titulo'],$articulo['subtitulo'],$articulo['categoria'],$articulo['condicion'],$articulo['descripcion'],$articulo['cantidad'],$articulo['precio'],$articulo['idus'],$articulo['imagen']);
		
		return $myArticulo;
	}
	public function buscar($palabra)
	{
		require_once('../connection.php');
		$db=Db::getConnect();
		$listaArticulos=[];
		$select=$db->prepare("SELECT * FROM articulo WHERE titulo LIKE :palabra OR subtitulo LIKE :palabra OR plantilla LIKE :palabra ");
		$select->bindValue('palabra','%'.$palabra.'%');
		$select->execute();
		
		foreach($select->fetchAll() as $articulo){
			$myArticulo= new Articulo($articulo['idarticulo'],$articulo['plantilla'],$articulo['titulo'],$articulo['subtitulo'],$articulo['categoria'],$articulo['condicion'],$articulo['descripcion'],$articulo['cantidad'],$articulo['precio'],$articulo['idus'],$articulo['imagen']);
			$listaArticulos[]=$myArticulo;
		}
		return $listaArticulos;
	}
	public function buscarCategoria($categoria)
	{
		require_once('../connection.php');
		$db=Db::getConnect();
		$listaArticulos=[];
		$select=$db->prepare('SELECT * FROM articulo WHERE categoria = :categoria ');
		$select->bindValue('categoria',$categoria);
		$select->execute();
		
		foreach($select->fetchAll() as $articulo){
			$myArticulo= new Articulo($articulo['idarticulo'],$articulo['plantilla'],$articulo['titulo'],$articulo['subtitulo'],$articulo['categoria'],$articulo['condicion'],$articulo['descripcion'],$articulo['cantidad'],$articulo['precio'],$articulo['idus'],$articulo['imagen']);
			$listaArticulos[]=$myArticulo;
		}
		return $listaArticulos;
	}
	public function eliminar($id){
		$db=Db::getConnect();
		$eliminar=$db->prepare('DELETE FROM articulo WHERE idarticulo=:id');
		$eliminar->bindValue('id',$id);
		$eliminar->execute();
	}

	//la función para registrar un usuario

}
?>