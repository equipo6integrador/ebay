<?php
/**
* Modelo para el acceso a la base de datos y funciones CRUD
* Autor: ELivar Largo
* Sitio Web: wwww.ecodeup.com
*/
class Usuario
{
	
	//constructor de la clase
	function __construct($id,$nombre, $apellidos, $email, $contrasena)
	{
		$this->id=$id;
		$this->nombre=$nombre;
		$this->apellidos=$apellidos;
		$this->email=$email;
		$this->contrasena=$contrasena;
	}
	

	//la función para registrar un usuario
	public static function save($usuario){
		$db=Db::getConnect();
		$insert=$db->prepare('INSERT INTO usuario VALUES(null,:nombre,:apellidos,:email, :contrasena)');
		$insert->bindValue('nombre',$usuario->nombre);
		$insert->bindValue('apellidos',$usuario->apellidos);
		$insert->bindValue('email',$usuario->email);
		$insert->bindValue('contrasena',$usuario->contrasena);
		$insert->execute();
	}

	//la función para registrar un usuario
	public static function login($nombre,$contrasena){
		$db=Db::getConnect();
		$insert=$db->prepare('SELECT * FROM usuario WHERE nombre=:nombre AND contrasena=:contrasena');
		$insert->bindValue('nombre',$nombre);
		$insert->bindValue('contrasena',$contrasena);
		$insert->execute();
		$usuarioDb=$insert->fetch();
		$usuario= new Usuario($usuarioDb['idusuario'],$usuarioDb['nombre'],$usuarioDb['apellidos'],$usuarioDb['email'],$usuarioDb['contrasena']);
		return $usuario;
	}

}
?>