<?php
session_start();
require('../Models/articulo.php');
$articulo = new Articulo(null,null,null,null,null,null,null,null,null,null,null);
$listaArticulos = $articulo->mostrar($_SESSION['id']);
?>
<!doctype html>
<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
    integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">

  <title>Hello, world!</title>
</head>

<body>
  <div class="container-fluid">
    <a href="../index.php"><img src="../img/ebaylogo.png" width="100" height="50" alt=""></a>
  </div>
  <div class="container">
    <br>
    <br>
    <h1> Tell us what you're selling </h1>
    <br>
    <div class="row">
      <div class="col-sm-10">
        <input type="text" class="form-control" name="buscar" placeholder="Buscar" align="center">
      </div>
      <div class="col-sm-2">
        <button type="button" class="btn btn-primary">Get Started</button>
      </div>
    </div>
    <br>
    <br>
    <br>
    <?php
     if(!empty($listaArticulos))
     {
        foreach ($listaArticulos as $articulo) {
          echo'
          <div class="card">
          <div class="card-body" style="padding-top:70px;">
            <div class = "row">
              <div class="col-sm-3">
                <img src="../img/'.$articulo->imagen.'" width="200" height="200" >
              </div>
              <div class = "col-sm-9">
                <h5 class="card-title">'.$articulo->titulo.'</h5>
                <p class="card-text">'.$articulo->descripcion.'</p>
                <a href="#">Resume</a> | <a href="../Controllers/articulo_controller.php?id='.$articulo->idarticulo.'&action=e">Delete</a>
              </div>
              
            </div>
           
          </div>
        </div>
        ';
        echo '<br>
              <br>';
          # code...
        }
     }
    ?>
    <div class="card">
      <div class="card-body" style="padding-top:70px;">
        <h5 class="card-title">Listing Template</h5>
        <p class="card-text">Create reusable listing templates to help you list items faster.</p>
        <a href="agregarProducto.php" class="btn btn-primary">Create a template</a>
      </div>
    </div>

  </div>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js"
    integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous">
  </script>
  <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js"
    integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js"
    integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous">
  </script>
</body>

</html>