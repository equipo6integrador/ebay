<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">

  <title>Inicia sesión o regístrate!</title>
</head>
<body>
  <div class="container">
    <div class="row flex-nowrap justify-content-between align-items-center py-4">
      <a class="text-muted" href="../index.php"><img src="../img/ebaylogo.png" width="70" height="35" alt=""></a>
      <h5>¿Ya eres un usuario? <a href="login.php">Inicia sesión</a></h5>
    </div>
    <br>
    <br>
    <h2 style="text-align: center;"> <b>Abrir una cuenta</b> </h2>  
    <p style="text-align: center;">¿Tienes un negocio? Crea una cuenta comercial</p>
    <br>
    <form id="frmRegistro" action="../Controllers/usuario_controller.php" method="post">
      <input type='hidden' name='action' value='register'>
      <div class="row flex-nowrap col-5 py-3">
        <input type="text" class="form-control" name="nombre" placeholder="Nombre" align="center">
        <input type="text" class="form-control" name="apellidos" placeholder="Apellidos" align="center">
      </div> 
      <div class="row flex-nowrap col-5 py-3" style="text-align: center;">
       <input type="text" class="form-control" name="email" placeholder="Correo Electronico" align="center">
     </div>
     <div class="row flex-nowrap col-5 py-3">
       <input type="password" class="form-control" name="contrasena" placeholder="Contraseña" align="center">
     </div>
     <div class="row flex-nowrap col-5">
       <p style="line-height: 80%;"><font size="2">Al crear una cuenta, aceptas nuestras Condiciones de Uso y admites haber leído nuestro Aviso de Privacidad.</font> </p>
     </div>
     <div class="row flex-nowrap col-5 ">
      <button type="submit" class="btn btn-primary btn-lg btn-block" align="center">Crear cuenta</button>
    </div>
  </form>                
</div>
<br>
<br>
<br>
<br>
<div>
  <p style="text-align: center;"><font size="2"> Copyright © 1995-2019 eBay Inc. Todos los derechos reservados. Condiciones de uso, Aviso de privacidad, cookies y AdChoice</font></p>
</div>                                         
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/main.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>