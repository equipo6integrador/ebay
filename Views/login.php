<?php
  session_start();
?>
<html lang="en">
<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">

  <title>Inicia sesión o regístrate!</title>
</head>
<body>
  <div class="container">
    <div class="row flex-nowrap justify-content-between align-items-center py-4">
      <a class="text-muted" href="../index.php"><img src="../img/ebaylogo.png" width="70" height="35" alt=""></a>
    </div>
    <br>
    <br>
    <h2 style="text-align: center;"> <b>Hola</b> </h2>  
    <p style="text-align: center;">Inicia sesión en eBay o <a href="registro.php">crea una cuenta</a></p>
    <br>
    <form id="frmLogin" action="../Controllers/usuario_controller.php" method="post">
      <input type='hidden' name='action' value='login'> 
      <label><font size="2">Nombre de usuario</font></label>
      <div class="row flex-nowrap col-5" style="text-align: center;">
       <input type="text" class="form-control" name="nombre" align="center">
     </div>
     <label><font size="2">Contraseña</font></label>
     <div class="row flex-nowrap col-5">
       <input type="password" class="form-control" name="contrasena" align="center">
     </div>
     <div class="row flex-nowrap col-5 py-4">
       <button type="submit" class="btn btn-primary btn-lg btn-block" align="center">Iniciar sesión</button>
     </div>
   </form>
      <?php
        if (isset($_SESSION['errorinicio'])) {
          echo '<p>Contraseña o usuario incorrecto </p>';
          # code...
        }
      ?>                
 </div>
 <br>
 <br>
 <br>
 <br>
 <div>
  <p style="text-align: center;"><font size="2"> Copyright © 1995-2019 eBay Inc. Todos los derechos reservados. Condiciones de uso, Aviso de privacidad, cookies y AdChoice</font></p>
</div>                                         
</div>


<!-- Optional JavaScript -->
<!-- jQuery first, then Popper.js, then Bootstrap JS -->
<script src="js/main.js"></script>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>