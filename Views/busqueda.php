
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Untitled</title>
    <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="../css/main.css">
</head>

<body class="">
    <header class="blog-header py-3">
    <form class="row flex-nowrap justify-content-between align-items-center"  action="../Controllers/articulo_controller.php" id="frmBusqueda" method="POST">
        <div class="col-2 pt-1">
          <a class="text-muted" href="../index.php"><img src="../img/ebaylogo.png" width="100" height="50" alt=""></a>
        </div>
          <input type="hidden" name="action" value="buscar">
          <div class="col-8 ">
            <input type="text" class="form-control" name="buscador" id="exampleInputPassword1" placeholder="Buscar articulo">
          </div>
          <div class="col-2 d-flex justify-content-end ">
            <button type="submit" class="btn btn-primary">Buscar</button>
          </div>
        </form>
        </header>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <ul class="list-unstyled">
                        <li><a href="../Controllers/articulo_controller.php?categoria=Antiguedades&action=sC" style="font-size: 25px;">Antiguedades</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Arte&action=sC" style="font-size: 25px;">Artes</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Bebes&action=sC" style="font-size: 25px;">Bebes</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Libros&action=sC" style="font-size: 25px;">Libros</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Negocios e industrias&action=sC" style="font-size: 25px;">Negocios e industrias</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Camaras o fotografias&action=sC" style="font-size: 25px;">Camaras o fotografias</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Celulares y accesorios&action=sC" style="font-size: 25px;">Celulares y accesorios</a></li>
                        <li><a href="../Controllers/articulo_controller.php?categoria=Ropa zapatos y accesorios&action=sC" style="font-size: 25px;">Ropa, Zapatos y accesorio</a></li>
                    </ul>
                </div>


                <div class="col-md-8">
                    <?php
                    if(!empty($listaArticulos))
                    {
                        foreach ($listaArticulos as $articulo) {
                            echo'
                            <div class="card">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-xl-3">
                                    <img src="../img/'.$articulo->imagen.'" width="200" height="200" >
                                </div>
                                <div class="col-xl-9">
                                    <h4><a href="../Controllers/articulo_controller.php?ida='.$articulo->idarticulo.'&action=s">'.$articulo->titulo.'</a></h4>
                                    <h6 class="text-muted mb-2">'.$articulo->condicion.'</h6>
                                    <p>'.$articulo->descripcion.'</p>
                                    <h1>MXN '.$articulo->precio.'</h1>
                                </div>
                            </div>
                        </div>
                    </div>
                          ';
                    }
                    }
                    else{
                        echo'<h1> No se encontro ningun articulo</h1>';   
                    }   
                    
                    ?>
                    

                </div>


            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
</body>

</html>