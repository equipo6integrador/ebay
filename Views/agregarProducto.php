<?php
session_start();
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" href="css/main.css">

    <title>Crea tu plantilla</title>
  </head>
  <body>
      <div class="container">
              <div class="row flex-nowrap justify-content-between align-items-center py-4">
                        <a class="text-muted" href="../index.php"><img src="../img/ebaylogo.png" width="140" height=60 alt=""></a>
                        <p>Envíenos sus comentarios</p>
              </div>
              <br>
              <h3> Crea tu plantilla </h3>  
              <p>Listado de detalles</p>
              <br>
			  <form id="frmProducto" action="../Controllers/articulo_controller.php" enctype="multipart/form-data" method="post">
			  <input type='hidden' name='action' value='registrar'>
              	<div class="row flex-nowrap col-12">
              		<label>*Nombre de la plantilla</label>
              		<input type="text" class="form-control" id="txtNombrePlantilla" name="plantilla" required>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-8" >
              		<label>*Titulo </label>
              		<input type="text" class="form-control" id="txtTitulo" name ="titulo" required>
              	</div>
              	<br>
             	<div class="row flex-nowrap col-8" >
              		<label>Subtitulo </label>
              		<input type="text" class="form-control" id="txtSubitulo" name="subtitulo" required>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-5" >
              		<label>*Categoria </label>
              		<select name="categoria" class="form-control" required>
              			<option value="0">Seleccione...</option>
              			<option value="Antiguedades">Antiguedades</option>
              			<option value="Arte">Arte</option>
              			<option value="Bebes">Bebes</option>
              			<option value="Libros">Libros</option>
              			<option value="Negocios e industrias">Negocios e Industrias</option>
              			<option value="Camaras o fotografias">Camaras o Fotografias</option>
              			<option value="Celulares y accesorios">Celulares y accesorios</option>
              			<option value="Ropa zapatos y accesorios">Ropa, Zapatos y accesorios</option>
              		</select>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-5" >
              		<label>*Condicion </label>
              		<select name="condicion" class="form-control" required>
              			<option value="0">Seleccione...</option>
              			<option value="Nuevo">Nuevo</option>
              			<option value="Paquete recien abierto">Paquete Recien Abierto</option>
              			<option value="Usado">Usado</option>
              			<option value="Por partes">No Funciona, Por Partes</option>
              		</select>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-12" >
              		<label>Descripcion </label>
              		<textarea class="form-control" name="descripcion" rows="5" cols="30" required></textarea>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-8" >
              		<label>*Cantidad de articulos a vender </label>
              		<input type="text" class="form-control" id="txtCantidadArticulos" name="cantidad" required>
              	</div>
              	<br>
              	<div class="row flex-nowrap col-8" >
              		<label>*Precio de venta unitario: $ </label>
              		<input type="text" class="form-control" id="txtPrecioArticulo" name="precio" required>
              	</div>
				  <div class="row flex-nowrap col-8" >
              		<label>*imagen:  </label>
              		<input type="file"  id="imagen" name="imagen" required>
              	</div>
              	<div>
				  <button type="submit" class="btn btn-primary btn-lg btn-block" align="center">Guardar Plantilla</button>
              	</div>
              </form>
                
              </div>
              <br>
              <br>
              <br>
              <br>
                    <div>
                          <p style="text-align: center;"><font size="2"> Copyright © 1995-2019 eBay Inc. Todos los derechos reservados. Condiciones de uso, Aviso de privacidad, cookies y AdChoice</font></p>
                    </div>                                         
              </div>
    

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="js/main.js"></script>
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
  </body>
</html>