<?php
session_start();
?>

<html lang="en">

<head>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
    integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
  <link rel="stylesheet" href="css/main.css">

  <title>Hello, world!</title>
</head>

<body>
  <div class="container-fluid">
    <nav class="navbar-xs navbar-light bg-light" ">
      <div class=" container">
      <?php 
        
        if (isset($_SESSION["nombre"]))
        {
          $nombre=$_SESSION['nombre'];
          $id = $_SESSION['id']; //Si existe agarra el nombre pa :D

          echo "<p>Bienvenido $nombre ".'<a href="Controllers/logout.php">Salir</a><span>| ebay Ofertas  |  <a href="Views/venta.php">Vender</a> |  Ayuda  y contacto</span></p>';
        }else {
          echo '<p><a href="Views/login.php">Inicia sesión</a> o <a href="Views/registro.php">Regístrate</a> <span>| ebay Ofertas  |  Vender |  Ayuda  y contacto</span></p>';
          
        } ?>

  </div>
  </nav>
  </div>
  <div class="container">

    <header class="blog-header py-3">
     
        
        <form class="row flex-nowrap justify-content-between align-items-center"  action="Controllers/articulo_controller.php" id="frmBusqueda" method="POST">
        <div class="col-2 pt-1">
          <a class="text-muted" href="#"><img src="img/ebaylogo.png" width="100" height="50" alt=""></a>
        </div>
          <input type="hidden" name="action" value="buscar">
          <div class="col-8 ">
            <input type="text" class="form-control" name="buscador" id="exampleInputPassword1" placeholder="Buscar articulo">
          </div>
          <div class="col-2 d-flex justify-content-end ">
            <button type="submit" class="btn btn-primary">Buscar</button>
          </div>
        </form>
        

    </header>
    <div id="myModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title">Disclaimer</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <p>Este es un proyecto Hecho para fines educativos todos los derechos reservados a ebay.com</p>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-primary">Save changes</button>
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>
    <div class="nav-scroller py-1 mb-2">
      <nav class="nav d-flex justify-content-between">
        <a class="p-2 text-muted" href="#">Inicio</a>
        <a class="p-2 text-muted" href="#">Tecnologia</a>
        <a class="p-2 text-muted" href="#">Moda</a>
        <a class="p-2 text-muted" href="#">Salud y Belleza</a>
        <a class="p-2 text-muted" href="#">Vehiculos</a>
        <a class="p-2 text-muted" href="#">Coleccion y Arte</a>
        <a class="p-2 text-muted" href="#">Equipo Industrial</a>
        <a class="p-2 text-muted" href="#">Deporte</a>
        <a class="p-2 text-muted" href="#">Hogar y Jardin</a>
      </nav>
    </div>
    <hr>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <img class="d-block w-100" src="img/Slider1.PNG" alt="First slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/Slider2.PNG" alt="Second slide">
        </div>
        <div class="carousel-item">
          <img class="d-block w-100" src="img/Slider3.PNG" alt="Third slide">
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <br>
    <br>
    <h3>Esto te puede interesar</h3>
    <div class="row ">
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle " width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Las mejores ofertas</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">NBA</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Devolucion de tu dinero</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">NFL</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle " width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Star Wars</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">LEGO</h5>
      </div>
    </div>
    <br>
    <br>
    <div class="jumbotron-fluid" style="background-color: #1dcbca; border-color: #1dcbca; height: 100px;">
      <h4 style="color: #121258;">Accede a ofertas exclusivas hoy mismo</h4>
      <p style="color: #121258;">Obtén gratis tu dirección de envíos en Estados Unidos</p>
    </div>
    <br>
    <br>
    <h3>Nuestros recomendados</h3>
    <div class="row ">
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle " width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Vendedores de Latinoamerica</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Justo lo que quieres</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Nintendo Switch Lite</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Relojes</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle " width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Sneakers de coleccion</h5>
      </div>
      <div class="col-sm-2">
        <svg class="bd-placeholder-img rounded-circle" width="140" height="140" xmlns="http://www.w3.org/2000/svg"
          preserveAspectRatio="xMidYMid slice" focusable="false" role="img" aria-label="Placeholder: 140x140">
          <title>Placeholder</title>
          <rect width="100%" height="100%" fill="#777" /><text x="50%" y="50%" fill="#777" dy=".3em">140x140</text>
        </svg>
        <h5 class="text-center">Aprende a vender</h5>
      </div>
    </div>
    <br>
    <br>
    <h3>eBay Ofertas</h3>
    <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
      <div class="carousel-inner">
        <div class="carousel-item active">
          <div class="row">
            <div class="col-3">
              <img class="d-block w-100" src="img/lap.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/cel.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/iphone.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/samsung.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
          </div>
        </div>
        <div class="carousel-item">
          <div class="row">
            <div class="col-3">
              <img class="d-block w-100" src="img/iphone.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/lap.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/samsung.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>
            <div class="col-3">
              <img class="d-block w-100" src="img/cel.jpg" alt="">
              <h5>MXN$3897</h5>
            </div>

          </div>
        </div>
      </div>
      <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
      </a>
      <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
      </a>
    </div>
    <br>
    <br>
    <div class="row jumbotron-fluid" style="height:40px ;">
      <div class="col-3" style="background-color: rgb(0, 49, 71);height:330px ;">
        <h4 style="color: #ffffff">Viste tu emocion</h4>
        <p style="color: #ffffff;">Camisetas autografiadas, gorras y más de tu equipo de la NFL</p>
      </div>
      <div class="col-9">
        <img src="img/nfl.PNG" alt="" height="80%">
      </div>
    </div>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="row jumbotron-fluid" style="height:40px ;">
      <div class="col-3" style="height:330px ;">
        <h4 style="color: #000000">Viste tu emocion</h4>
        <p style="color: #000000;">Camisetas autografiadas, gorras y más de tu equipo de la NFL</p>
      </div>
      <div class="col-9">
        <img src="img/stereo.PNG" alt="" height="75%">
      </div>
    </div>





  </div>
  <script type="text/javascript">
    $(function(){
     $("#myModal").modal();
    });
   </script>
  <script type="text/javascript">
  $( document ).ready(function() {
    $('#myModal').modal('toggle')
});
  </script>

  <!-- Optional JavaScript -->
  <!-- jQuery first, then Popper.js, then Bootstrap JS -->
  <script src="js/main.js"></script>
  <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
    integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
  </script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
    integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
  </script>
  <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
    integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
  </script>
  <?php
error_reporting(0);
 require_once('routes.php'); ?>
</body>

</html>