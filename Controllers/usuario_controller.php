<?php 
	/**
	* Descripción: Controlador para la entidad usuario
	* Autor: Elivar Largo
	* Web: www.ecodeup.com
	* Fecha: 25-02-2017
	*/
	session_start();
	class UsuarioController
	{	
		public function __construct(){}

		

		public function register(){
			require_once('../Views/Usuario/registro.php');
		}

		//guardar
		public function save($usuario){
			Usuario::save($usuario);
			header('Location: ../index.php');
		}

		//login
		//guardar
		public function login($nombre,$contrasena){
			Usuario::login($nombre,$contrasena);
			header('Location: ../index.php');
		}

		public function error(){
			require_once('Views/Usuario/error.php');
		} 
	}


	//obtiene los datos del usuario desde la vista y redirecciona a UsuarioController.php
	if (isset($_POST['action'])) {
		$usuarioController= new UsuarioController();
		//se añade el archivo usuario.php
		require_once('../Models/usuario.php');
		
		//se añade el archivo para la conexion
		require_once('../connection.php');

		if ($_POST['action']=='register') {
			$usuario= new Usuario(null,$_POST['nombre'],$_POST['apellidos'],$_POST['email'],$_POST['contrasena']);
			$usuarioController->save($usuario);
		}
		if ($_POST['action']=='login') {
			$nombre=$_POST['nombre'];
			$contrasena=$_POST['contrasena'];
			$usuario=Usuario::login($nombre,$contrasena);
			if($usuario->nombre == '')
			{
				$_SESSION['errorinicio']=true;
				header('Location: ../Views/login.php');
			}
			else {
				$_SESSION["nombre"] = $usuario->nombre;
				$_SESSION['id'] = $usuario->id ;
				header('Location: ../index.php');
			}
			//$usuarioController->login($nombre,$contrasena);
		}	
	}

	//se verifica que action esté definida
	if (isset($_GET['action'])) {
		if ($_GET['action']!='register'&$_GET['action']!='index') {
			require_once('connection.php');
			$usuarioController=new UsuarioController();
			//para eliminar
			if ($_GET['action']=='delete') {		
				$usuarioController->delete($_GET['id']);
			}elseif ($_GET['action']=='update') {//mostrar la vista update con los datos del registro actualizar
				require_once('../Models/usuario.php');				
				$usuario=Usuario::getById($_GET['id']);		
				//var_dump($usuario);
				//$usuarioController->update();
				require_once('../Views/Usuario/update.php');
			}	
		}	
	}
	?>