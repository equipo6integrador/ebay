<?php 
	/**
	* Descripción: Controlador para la entidad articulo
	* Autor: Elivar Largo
	* Web: www.ecodeup.com
	* Fecha: 25-02-2017
	*/
	session_start();
	class ArticuloController
	{	
		public function __construct(){}


		//guardar
		public function save($articulo){
			Articulo::save($articulo);
			header('Location: ../Views/venta.php');
		}

		public function delete($id){
			Articulo::eliminar($id);
			header('Location: ../Views/venta.php');
		}
		public function buscar($palabra)
		{
			$articulo = new Articulo(null,null,null,null,null,null,null,null,null,null,null);
			$listaArticulos = $articulo->buscar($palabra);
			//$listaArticulos = Articulo::buscar($palabra);
			require_once('../Views/busqueda.php');
		}
		public function buscarCategoria($categoria)
		{
			$articulo = new Articulo(null,null,null,null,null,null,null,null,null,null,null);
			$listaArticulos = $articulo->buscarCategoria($categoria);
			//$listaArticulos = Articulo::buscar($palabra);
			require_once('../Views/busqueda.php');
		}
		public function buscarId($id)
		{
			$articulo = new Articulo(null,null,null,null,null,null,null,null,null,null,null);
			$articulo2 = $articulo->buscarId($id);
			require_once('../Views/producto.php');
		}

		public function error(){
			require_once('Views/Usuario/error.php');
		} 
	}


	//obtiene los datos del usuario desde la vista y redirecciona a UsuarioController.php
	if (isset($_POST['action'])) {
		$articuloController= new ArticuloController();
		//se añade el archivo usuario.php
		require_once('../Models/articulo.php');
		
		//se añade el archivo para la conexion
		require_once('../connection.php');

		if ($_POST['action']=='registrar') {
		// Recibo los datos de la imagen
		$nombre_img = $_FILES['imagen']['name'];
		$tipo = $_FILES['imagen']['type'];
		$tamano = $_FILES['imagen']['size'];
		
		//Si existe imagen y tiene un tamaño correcto
		if (($nombre_img == !NULL) && ($_FILES['imagen']['size'] <= 200000))
		{
		//indicamos los formatos que permitimos subir a nuestro servidor
		if (($_FILES["imagen"]["type"] == "image/gif")
		|| ($_FILES["imagen"]["type"] == "image/jpeg")
		|| ($_FILES["imagen"]["type"] == "image/jpg")
		|| ($_FILES["imagen"]["type"] == "image/png"))
		{
			// Ruta donde se guardarán las imágenes que subamos
			$directorio = $_SERVER['DOCUMENT_ROOT'].'../mvc/img/';
			// Muevo la imagen desde el directorio temporal a nuestra ruta indicada anteriormente
			move_uploaded_file($_FILES['imagen']['tmp_name'],$directorio.$nombre_img);
			}
			else;
			{
			//si no cumple con el formato
			echo "No se puede subir una imagen con ese formato ";
			}
		}
		else
		{
		//si existe la variable pero se pasa del tamaño permitido
		if($nombre_img == !NULL) echo "La imagen es demasiado grande ";
		}
			$articulo= new Articulo(null,$_POST['plantilla'],$_POST['titulo'],$_POST['subtitulo'],$_POST['categoria'],$_POST['condicion'],$_POST['descripcion'],$_POST['cantidad'],$_POST['precio'],$_SESSION['id'],$nombre_img);
			$articuloController->save($articulo);
		}
		if($_POST['action'] == 'buscar'){
			$articuloController->buscar($_POST['buscador']);
		}
	}

	//se verifica que action esté definida
	if (isset($_GET['action'])) {
		if ($_GET['action']!='register'&$_GET['action']!='index') {
			require_once('../connection.php');
			$articuloController=new ArticuloController();
			require_once('../Models/articulo.php');
			//para eliminar
			if ($_GET['action']=='e') {		
				$articuloController->delete($_GET['id']);
			}
			if($_GET['action']=='s')
			{
				$articuloController->buscarId($_GET['ida']);
			}
			if ($_GET['action']== 'sC'){
				$articuloController->buscarCategoria($_GET['categoria']);
			}	
		}	
	}
	?>